---
title: ゆるネイティブでGo言語
paginate: true
---

# ゆるネイティブでGo言語

2022-07 by Yuki Nakamura

![Slides are here](images/qrcode.png)

![bg right:33% 50%](images/gopher.svg)

---

## 自己紹介

- 名前: Yuki Nakamura
- Go歴: 2014年（Go 1.4あたり）から
- 守備範囲: 主にバックエンド（APIなど）

---

## 今日話す内容

- Goについて
  - Goの特徴
  - どのような分野でよく使われているのか
- Goのはじめかた
  - Playground
  - 環境の準備
  - Goの文化
  - プロジェクト構成

だいたい2時間程度でやっていきます。

---

## 今日ターゲットにする人

- プログラマ、コードを書くエンジニア
- 新しくプログラミング言語を学んでみたい
- Java等に代わる軽くてシンプルな言語を探している
- クロスプラットフォームなツールを作りたい
- スクリプト言語のポータビリティに悩んでいる
- Googleが好きな人

**まずはGoを知ってもらい、入り口に立っていくことが目的です。**
**細かい文法や実装方法などの解説は行いません。**

---

# Goの概要

---

## Goとは

- [公式サイト](https://go.dev/) → https://go.dev/
- 2009年頃にGoogleによってOSSとして公開された
- [シンプルさにこだわった設計](https://talks.golang.org/2015/simplicity-is-complicated.slide#1)
- 静的型付け
- ガベージコレクション
- 並列プログラミング
- 充実したエコシステム
- シングルバイナリ
- クロスコンパイル
- WebAssembly(WASM)対応

「`Go言語`」「`Golang`」などと表記されることが多いが、正式名称は「`Go`」

---

## Goの目標・設計思想

- 静的に型付けされ、巨大なシステムでもスケールする
- 動的言語のように生産性が高く、リーダブルである
- IDEが必須ではない
- ネットワーク、並列処理をサポートする
- コンパイルが高速である
- デプロイが容易である
- 言語をシンプルに保つ

参考: [公式サイトのFAQ](https://go.dev/doc/faq#Origins)

---

### シンプルさにこだわった設計

- 言語仕様がシンプル
  - 予約語は25個
    参考: C: 32個、Java: 51個、Python: 36個、Ruby: 41個
  - 習得しやすい
  - 今どきの言語に慣れてる人は、物足りないかもしれない
- 効率的な動作（高速、メモリ効率）
- コンパイルが高速
- 他の言語の機能を積極的に取り入れたりしない
  - シンプルさが失われる
  - 新しい言語を作った意義がなくなる

---

### Goのコード（Hello World）

```go
package main

import "fmt"

func main() {
    fmt.Println("Hello, Go World!!")
}
```

---

### Goのコード（HTTPサーバー）

```go
package main

import (
    "fmt"
    "net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "Hello, World")
}

func main() {
    addr := ":8080"

    http.HandleFunc("/", handler)
    http.ListenAndServe(addr, nil)
}
```

---

### Goにないもの

- オブジェクト
- クラス
- プロトタイプ
- 継承
- 例外
- オーバーロード
- ~~ジェネリクス~~
  - Go 1.18でジェネリクスが追加されました🎉

……あれ、もしかしてGoってオブジェクト指向言語ではない？

参考: [Goのなぜ問答](https://zenn.dev/nobonobo/articles/9a9f12b27bfde9)

---

### オブジェクト指向プログラミングはできる

- 構造体に関連付いた関数を作れる
  - 実質的にインスタンスメソッド
- インターフェースがある
- コンポジションを活用する（継承の代わり）

参考: [Goはオブジェクト指向言語だろうか？ | POSTD](https://postd.cc/is-go-object-oriented/)

---

#### コード例

```go
// インターフェースの定義
type Shape interface {
    area() float64
}

// 構造体の定義
// このインターフェースを実装します、みたいな宣言はない
type Rectangle struct {
    width, height float64
}

// Rectangle構造体のメソッド定義
// インターフェースの定義を満たすだけで、自動的に認識される
func (r Rectangle) area() float64 {
    return r.width * r.height
}

type Circle struct { radius float64 }
func (c Circle) area() float64 { return 3.14 * c.radius * c.radius }

// Shapeインターフェースで受け取る関数
func printArea(name string, s Shape) {
    fmt.Printf("%sの面積: %.2f\n", name, s.area())
}
```

https://go.dev/play/p/vphYfloFrF2

---

#### コンポジションの例

```go
type Person struct {
    Name string
}

func (p Person) Greeting() {
    fmt.Printf("Hi, I'm %s.", p.Name)
}

type LoginUser struct {
    Person          // ← Personを内包させる
    ID       int
    Email    string
    Password string
}

func main() {
    user := LoginUser{
        Person:   Person{Name: "Alice"},
        ID:       1,
        Email:    "alice@example.com",
        Password: "SuperSecret!!!",
    }

    fmt.Println("Name:", user.Name) // ← PersonのNameにアクセスできてる
    user.Greeting()                 // ← PersonのGreeting()を呼び出せてる
}
```

https://go.dev/play/p/6CJUOzmK8_E

---

<!--
### 静的型付け

- 変数とか引数に型がある
- 型はコンパイル時にチェックされる
  - ミスを早期発見できる
  - なんなら、エディタ上でリアルタイムにわかる
- 大規模プログラミングやシステムプログラミングに有利

<hr />

最近は型のある言語が注目されている。**型は大事**。

[Python][py-typing]、[Ruby][rb-rbs]のように、動的型付け言語であるものに型注釈を付けたりといった動きも活発。

[py-typing]: https://docs.python.org/ja/3/library/typing.html
[rb-rbs]: https://github.com/ruby/rbs

---

### ガベージコレクション

- プログラマがメモリー管理に気を配らなくてよい
  - メモリーリーク、二重解放、無効なポインタ
- GoのGCは`Concurrent Mark & Sweep(CMS)`ベース
  - わりと工夫されていて、Stop the World(StW)が起きにくい（らしい）

---
-->

### 並列プログラミング

- 並列処理のための機能を言語仕様として持っている
- シンプルに扱える
- goroutine（ゴルーチン）
  - Goのランタイムが管理してくれる軽量スレッド
  - コンテキストスイッチが軽い
  - いい感じにマルチコアを使ってくれる
  - [coroutine](https://ja.wikipedia.org/wiki/%E3%82%B3%E3%83%AB%E3%83%BC%E3%83%81%E3%83%B3)をもじったものと思われる
- channel（チャネル）
  - 非同期処理で安全にデータをやり取りするための仕組み

---

#### goroutineの使い方

```go
func main() {
    // 普通の関数呼び出し
    greeting()

    // goroutineで関数呼び出し
    go greeting()
}

func greeting() {
    fmt.Println("Hello!")
}
```

関数呼び出しの前に`go`を付けるだけ！

<small>※この例は`main`関数がすぐに終わってしまうので、あまり意味はないです。
ちゃんとした版→ https://go.dev/play/p/chTIrm3kzCO </small>

---

#### 5000個のゴルーチンで円周率を計算する例

```go
package main

import (
    "fmt"
    "math"
)

func main() {
    fmt.Println(pi(5000)) // ← goroutine 5000個作る気だ！
}

func pi(n int) float64 {
    ch := make(chan float64) // ← データ受け渡し用のチャネル
    for k := 0; k < n; k++ {
        go term(ch, float64(k)) // ← ゴルーチンの生成
    }
    f := 0.0
    for k := 0; k < n; k++ {
        f += <-ch // ← チャネルからデータを受け取る
    }
    return f
}

func term(ch chan float64, k float64) {
    ch <- 4 * math.Pow(-1, k) / (2*k + 1) // ← 計算した値をチャネルに送る
}
```

https://go.dev/play/p/V3eZ9SQZUfi

---

### その他の特徴

- 複数の戻り値（多値）を返せる
  - メインの戻り値の他にエラーを返したりするのに使う
  - 例外がないので、最後の戻り値でエラーを返す文化
- ゼロ値
  - `nil`はポインタ型のみ
  - 未初期化の変数はゼロ値という値で初期化される
    - 数値なら`0`、文字列なら`""`（空文字列）、真偽値なら`false`など
  - ちょっと取り扱い注意なこともある
    - デフォルト値として使いづらい
    - 未入力なのか`0`なのか区別できない、など

---

#### 複数の戻り値の例

```go
func doSomething(value string) (string, error) {
    return "", errors.New("エラーです")
}

func main() {
    result, err := doSomething("hello")
    if err != nil {
        fmt.Println(err)
    }
}
```

---

### 充実したエコシステム

- 標準ライブラリ
  - 圧縮、暗号化、JSON、CSV、通信、テンプレートエンジン、など
  - Go自身に関するライブラリ（AST、フォーマッタ等）
- モジュール機能（パッケージ機能）
  - 依存性解決
  - VCSのリポジトリがそのままモジュールになる
- ツール
  - テスト
  - ドキュメント生成
  - フォーマッタ、 Lint（構文チェック）
  - LSP(Language Server Protocol)、公式のVSCode機能拡張

これらのものが最初から**公式**で揃っている。

---

### シングルバイナリ

- 実行ファイル1つだけ
- ライブラリ（`.so`や`DLL`など）への依存がない
  - もちろん、それらをリンクして使うこともできる
- デプロイが容易
  - ビルドした実行ファイルを1つだけコピーすればよい
  - 実行環境に予めGoやライブラリ等をインストールしておく必要なし！

---

#### 依存ライブラリなし

```console
$ cat >main.go
package main

import "fmt"

func main() {
    fmt.Println("Hello, Go World!!")
}

$ go build

$ ./hello
Hello, Go World!!

$ file hello
hello: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), statically linked, not stripped

$ ldd main.go
	not a dynamic executable
```

---

#### 依存ライブラリがある実行ファイルの例

```console
$ ldd /bin/ls
	linux-vdso.so.1 (0x00007fff77149000)
	libselinux.so.1 => /lib/x86_64-linux-gnu/libselinux.so.1 (0x00007f689af46000)
	libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f689ad54000)
	libpcre2-8.so.0 => /lib/x86_64-linux-gnu/libpcre2-8.so.0 (0x00007f689acc4000)
	libdl.so.2 => /lib/x86_64-linux-gnu/libdl.so.2 (0x00007f689acbe000)
	/lib64/ld-linux-x86-64.so.2 (0x00007f689af9a000)
	libpthread.so.0 => /lib/x86_64-linux-gnu/libpthread.so.0 (0x00007f689ac9b000)
```

---

### クロスコンパイル

- 開発環境（OS）とは別の環境向けの実行ファイルをビルドすること
  - 例: Windows上でLinux用の実行ファイルを生成する
- Goは特別な準備なしに**簡単に**クロスコンパイルが行える
  - クロスコンパイルに必要なものが最初から準備されている
    →**追加でなにかインストールしたりする必要なし**
  - ビルド時にターゲットを環境変数で設定するだけ
    →**難しい設定とか必要なし**

---

#### クロスコンパイルしてみる

```sh
# 普通のビルド（同じOS向け）
go build

# Windows向けビルド
GOOS=windows GOARCH=amd64 go build

# macOS向けビルド
GOOS=darwin GOARCH=amd64 go build

# Linux向けビルド
GOOS=linux GOARCH=amd64 go build

# WebAssembly!!!
GOOS=js GOARCH=wasm go build
```

環境変数`GOOS`でOS、`GOARCH`でアーキテクチャ（CPU）の種類を指定するだけ！

---

#### 対応環境一覧（Go 1.18時点）

```console
$ go version
go version go1.18.3 darwin/amd64

$ go tool dist list
aix/ppc64       js/wasm         netbsd/arm
android/386     linux/386       netbsd/arm64
android/amd64   linux/amd64     openbsd/386
android/arm     linux/arm       openbsd/amd64
android/arm64   linux/arm64     openbsd/arm
darwin/amd64    linux/mips      openbsd/arm64
darwin/arm64    linux/mips64    openbsd/mips64
dragonfly/amd64 linux/mips64le  plan9/386
freebsd/386     linux/mipsle    plan9/amd64
freebsd/amd64   linux/ppc64     plan9/arm
freebsd/arm     linux/ppc64le   solaris/amd64
freebsd/arm64   linux/riscv64   windows/386
illumos/amd64   linux/s390x     windows/amd64
ios/amd64       netbsd/386      windows/arm
ios/arm64       netbsd/amd64    windows/arm64
```

----

### スクリプト言語のような扱いやすさ

- シンプルな言語仕様で扱いやすい
- 豊富な標準ライブラリ
  - 追加でパッケージとかインストールしなくてもだいたいのことはできる
- 開発環境のセットアップが簡単
  - 専用のIDEとか不要
    - [GoLand](https://www.jetbrains.com/ja-jp/go/)のようなすごいIDEもあるが、[VSCode](https://code.visualstudio.com/)で十分
  - 極端な話テキストエディタだけでもいい
- シングルバイナリなので、気軽にコピーして配置できる
- 簡単にクロスコンパイルできる

---

## どのような分野で使われているか

- Webアプリケーション、Webシステム
  - APIバックエンド、WebAssembly、Webサーバー、Proxyサーバー
- DockerやKubernetes等のコンテナ系プロダクト
- パフォーマンス重視の処理
  - マイクロサービス等のサーバー間連携など（gRPC）
  - 動画処理、画像処理など
- AI・機械学習
  - [TensorFlow](https://www.tensorflow.org/)、[Gorgonia](https://gorgonia.org/)、[ONNX](https://onnx.ai/)、[GoLearn](https://www.golearn.guru/)
- ロボティクス（ドローンやロボットの制御）
  - [Gobot](https://gobot.io/)というロボティクス用フレームワークがある
- ユーティリティ等のコマンドラインツール

---

## 得意ではないけど、存在している分野

- モバイルアプリ（スマホアプリ）
  - 理論的には作れるが、全然向いてない or 未発達
  - スマホのOS上で実行可能なコードが生成できるというだけで、GUIとかそういうパーツが揃っているわけではない
- コンシューマーゲーム機
  - [ニンテンドースイッチのゲーム作ったツワモノもいるらしい](https://zenn.dev/hajimehoshi/articles/72f027db464280)
    - しかも[ニンテンドーeショップ](https://store-jp.nintendo.com/list/software/70010000041018.html)で販売してる！
- IoT、組み込みシステム
  - 基本的にGoは組み込みには向いてない（GCやフットプリント）
    - 家電とか産業機械みたいなガチな組み込みは無理
    - [TinyGo](https://tinygo.org/)という組み込み向けのサブセットを作るプロジェクトもある

---

## 有名なプロダクト

- [Docker](https://www.docker.com/)
- [Kubernetes](https://kubernetes.io/)
- [CockroachDB](https://www.cockroachlabs.com/)
- [Hugo](https://gohugo.io/)
- [Terraform](https://www.terraform.io/)
- [Caddy](https://caddyserver.com/)
- [Snappy](https://snapcraft.io/)
- Go (!?)

https://en.wikipedia.org/wiki/Go_(programming_language)#Applications

---

## GoもGoで作られている

- [ブートストラッピング](https://ja.wikipedia.org/wiki/%E3%83%96%E3%83%BC%E3%83%88%E3%82%B9%E3%83%88%E3%83%A9%E3%83%83%E3%83%97%E5%95%8F%E9%A1%8C)、[セルフホスティング](https://ja.wikipedia.org/wiki/%E3%82%BB%E3%83%AB%E3%83%95%E3%83%9B%E3%82%B9%E3%83%86%E3%82%A3%E3%83%B3%E3%82%B0)
- Goのツールチェイン（コンパイラや同梱ツール）はGoで書かれている
  - Go 1.4まではCで書かれていた
- Go言語自体を扱うライブラリ（構文解析など）も標準で持っている
  - フォーマッタとかLintなどのツールが作りやすい
- 容易なクロスコンパイルを実現
  - 普通はターゲット環境用のツールチェイン（`gcc`とかOSのライブラリとか）を用意しないといけない

---

## Goを採用している有名な企業（抜粋）

<div style="margin-bottom:1rem; column-count:2">

- Google
- Meta (Facebook)
- Cloudflare
- Dropbox
- Netflix
- Uber
- メルカリ
- LINE
- ぐるなび
- クックパッド
- ミクシィ
- DeNA

</div>

わざわざ「Goを採用しています」とは言わなくなってきている。
メインで使ってるかどうかはともかく、どこかしらで使っていたりする。

---

# Goのはじめかた

---

## 公式サイト・公式ドキュメント

https://go.dev/

- 必要なものは全部揃っている
- インストーラー
- [チュートリアル](https://go.dev/tour/)
- [Playground](https://go.dev/play/)
- [Goのドキュメント](https://go.dev/doc/)
  - ドキュメント内のサンプルコードは実行可能
- [パッケージのドキュメント](https://pkg.go.dev/)
  - 公式だけじゃなくて、GitHub等で公開されてるパッケージも！

---

### A Tour of Go

https://go.dev/tour/

- チュートリアル
- 基本的なコードについて学べる
- ブラウザ上で実行しながら学べる

まずはここでGoのコードはどんな感じなのか感触を掴むとよい。

---

### Playground

https://go.dev/play/

- ブラウザ上で実行できる環境
- ちょっとしたコードを書いてみるのに最適
  - 本や記事上のサンプルコードを試してみる
  - 書き方の勉強
- コードの共有ができる
  - 他人に見せたり教えたり

※本スライドのサンプルコードにも使った。

---

## 環境の準備

- Goのインストール
- エディタの用意
- Git
  - モジュールの取得時に使用される
- コマンドライン環境
  - macOS: `Termina.app`、[iTerm2](https://iterm2.com/)
  - Windows: `コマンドプロンプト`、[Windows Terminal](https://apps.microsoft.com/store/detail/windows-terminal/9N0DX20HK701?hl=ja-jp&gl=JP)
  - Visual Studio Codeの中のターミナル

---

### インストール

- [公式パッケージ](https://go.dev/dl/)
- 各種OSのパッケージマネージャ
  - yum
  - apt
  - [Homebrew](https://formulae.brew.sh/formula/go)
  - [winget](https://winget.run/pkg/GoLang/Go)
  - [Scoop](https://scoop.sh/#/apps?q=go&s=0&d=1&o=true)
  - [Chocolatey](https://community.chocolatey.org/packages/go)

---

### エディタ

- [Visual Studio Code(VSCode)](https://code.visualstudio.com/) + [Go拡張機能](https://marketplace.visualstudio.com/items?itemName=golang.Go)
- [GoLand](https://www.jetbrains.com/ja-jp/go/) by JetBrains ※有料
- お好きなテキストエディタ
  - LSP(Language Server Protocol)に対応しているとなおよい
    - 入力補完や定義の参照など、IDEがよく持っている機能を切り出して標準化したもの
    - Goは公式で`gopls`というLSP実装を提供している
  - Vim/NeovimでGo書く人も多いとかなんとか

スクリプト言語みたいに、普通のテキストエディタでもどうにかなる。

---

## Goのルール

- 大文字から始まる定義が`public`の可視性で小文字が`private`
- 未使用の変数やインポートは許容されない
- モジュール（パッケージ）はVCSのリポジトリをそのまま使う
  - 専用のパッケージ機構はない
- ファイル名のルール
  - `.`、`_`で始まるファイル・ディレクトリはコンパイル対象外
  - `testdata`というディレクトリはコンパイル対象外
  - `_test.go`で終わるファイルはテスト時のみコンパイルされる（例: `foo_test.go`）
  - 他にも特定のターゲット用にファイル名ルールなどがある

---

## Goのモジュール

- モジュールはVSCのリポジトリをそのまま使う
- 参照するバージョンやリポジトリのタグ等は`go.mod`ファイルで管理される

```go
import "github.com/spf13/cobra"

import (
    "github.com/fsnotify/fsnotify"
    foo "github.com/foo/foo/v2"
)
```

バージョンは`go get`する時に指定可能。

```sh
go get github.com/bar/bar@v2.0.0
```

[Go Modules Reference - The Go Programming Language](https://go.dev/ref/mod)

---

### 相対パスでは`import`できない

```go
// github.com/myname/myapp を作っているとする
package myapp

import (
    // OK
    "github.com/myname/myapp/bar"

    // NG
    "./bar"
)
```

---

## Goの文化

- MUST
    - `gofmt`をかける
    - `panic`を（可能な限り）使わない
      - 例外の代わりに使わない
    - `error`を握りつぶさない
- 他にもいろいろ
  - 参考: [鵜飼文敏氏「Goに入ってはGoに従え」可読性のあるコードにするために～Go Conference 2014 Autumn基調講演2人目：Go Conference 2014 Autumnレポート｜gihyo.jp … 技術評論社](https://gihyo.jp/news/report/01/GoCon2014Autumn/0002) / [スライド](http://ukai-go-talks.appspot.com/2014/gocon.slide#1)

---

### Goの文化を支える技術

- 最初からある程度の統一された文化が出来上がっている
- 公式によるお作法
  - [チュートリアル集](https://go.dev/doc/)
  - [Go Code Review Comments](https://github.com/golang/go/wiki/CodeReviewComments)
  - [Effective Go](https://go.dev/doc/effective_go)
  - `gofmt`（公式のコードフォーマッター）
  - `golint`、`go vet`（公式の静的解析ツール）
    - 潜在的にバグになり得るコードや、行儀の良くない書き方のチェック
- わりと見かける習慣
  - タスクランナーには`make`(`Makefile`)を使う

他の言語だと、お作法自体がいくつもあったり、オレオレルールを作ったりすることが多い。

---

### Goに入ってはGoに従え

- 統一されたお作法があるので、みんなそれに従いましょうね、ということ
- 他の言語の流儀や自分の好みを勝手に適用しない🙅
- Go Wayに従っていないコードは受け入れてもらえない
- 公式のお作法ドキュメント（前述）を読む
- Go自身のコードも参考になる
  - 標準ライブラリのコードとか
  - 実際、それらを手本にした解説記事なども多い

---

## プロジェクト構成

---

### 簡単なプロジェクト構成（ディレクトリレイアウト）

```
myapp
├── .git            …… Git
├── .gitignore      …… Git
├── bar             …… barパッケージ
│   ├── bar.go      …… barパッケージのコード
│   └── bar_test.go …… barのテストコード
├── foo.go          …… mainパッケージのその他のコード
├── foo_test.go     …… fooのテストコード
├── go.mod          …… Goのモジュール情報
└── main.go         …… main関数を含むコード
```

- トップレベルのコードは`package main`にする。
- `src`とか`bin`は作らない。

---

### ライブラリの場合

```
mylib
├── .git                …… Git
├── .gitignore          …… Git
├── cmd                 …… コマンドアプリ用ディレクトリ
│   └── myapp           …… myappコマンド用ディレクトリ
│       └── main.go     …… myappコマンドのコード
├── bar                 …… mylib/barパッケージ
│   └── bar.go          …… mylib/barパッケージのコード
├── mylib.go            …… mylibパッケージのコード
└── go.mod              …… Goのモジュール情報
```

- トップレベルのコードを`package mylib`にする。
- 実行可能なコマンドを含める場合は`cmd/<name>`にコードを置く。
- 参考: https://github.com/peco/peco

---

## プロジェクトの作り方

https://go.dev/doc/tutorial/getting-started

```sh
# ディレクトリ作成
mkdir myapp
cd myapp

# Gitリポジトリ初期化
git init
gibo dump linux macos windows jetbrains visualstudiocode go >>.gitignore

# Goモジュールの初期化
go mod init myapp
# 外部からURLでインポートさせる場合はリポジトリのURLに合わせる
#go mod init github.com/myname/myapp

# コードを書く
$EDITOR main.go
```

参考: [gibo](https://github.com/simonwhitaker/gibo)…….gitignoreの生成ツール

---

## 開発の進め方

- コードを書く
- `go run`で実行してみる（コンパイルせずに直接実行できる）
- テストコードを書く
- `go test`でテストを実行する
- `go build`でビルドする

---

## リリースビルドの作り方

- `go build -ldflags="-s -w" -trimpath`でビルドする
  - `-ldflags="-s -w"`はデバッグ用のシンボルを削除するオプション（ファイルサイズが小さくなる）
  - `-trimpath`はバイナリから開発環境のファイルパス等を取り除くオプション
  - 最初は**おまじない**だと思っておけばいい
  - 参考: [Goメモ-111 (ビルド時にデバッグ情報とかを消してサイズを小さくする, ldflags, trimpath, upx) - いろいろ備忘録日記](https://devlights.hatenablog.com/entry/2020/10/21/192615)
- 環境変数`GOOS`、`GOARCH`を指定して、Linux、macOS、Windows用のビルドを作る
    - [gox](https://github.com/mitchellh/gox)を使うと一発で全部ビルドできる

---

## 情報を探すときの注意点

### `GOPATH`と`Module-aware mode`(`GO111MODULE`)

- Goの開発方式には`GOPATH mode`と`Module-aware mode`というのがある
- `GOPATH`という環境変数を使った開発方式は**古い**方式
  - 「`GOPATH`を設定して云々」みたいなこと書いてある記事は**古い記事**
- 対して`Module-aware mode`を設定する`GO111MODULE`もある
  - 今はデフォルトで有効なので、`GO111MODULE`を`on`に設定して……という記述もやや古い

今はこのような環境変数は一切設定する必要なし。
（`PATH`にGoのコマンドを追加する、などは必要）

---

### `go get`と`go install`

- `go get`
  - Goのプロジェクト内で、使用する外部モジュールを取得するコマンド
- `go install`
  - Goで作られたアプリをインストールするコマンド
  - `go install github.com/foo/bar@v1.0.0`とかすると`bar`コマンドの`v1.0.0`がインストールできる

以前は両方の用途に`go get`が使われていた。
古い記事では「`go get`でインストールしましょう」みたいなことが書かれているかもしれないので、`go install`に読み替える。

参考: [Go1.16からは go get は使わず go install を使おう - Qiita](https://qiita.com/eihigh/items/9fe52804610a8c4b7e41)

---

## 参考リンク

- [公式サイト](https://go.dev/)
  - [Getting Started](https://go.dev/doc/tutorial/getting-started)
  - [A Tour of Go](https://go.dev/tour/)
  - [Playground](https://go.dev/play)
  - [Documentation](https://go.dev/doc/)
  - [パッケージ](https://pkg.go.dev/)
- [公式リポジトリ](https://github.com/golang/go)
  - [Wiki](https://github.com/golang/go/wiki)
- [Awesome Go](https://github.com/avelino/awesome-go)

---

## 書籍紹介

### 入門

- [『たった1日で基本が身に付く！ Go言語 超入門』](https://gihyo.jp/book/2020/978-4-297-11617-0)
- [『入門Goプログラミング』](https://www.shoeisha.co.jp/book/detail/9784798158655)
- [『プログラミング言語Go』](https://www.maruzen-publishing.co.jp/item/?book_no=295039)

### ノウハウ

- [『改訂2版 みんなのGo言語』](https://gihyo.jp/book/2019/978-4-297-10727-7)
- [『エキスパートたちのGo言語 一流のコードから応用力を学ぶ』](https://gihyo.jp/book/2022/978-4-297-12519-6)
- [『Go言語による並行処理』](https://www.oreilly.co.jp/books/9784873118468/)

---

# おわり

おつかれさまでした😃
