const path = require("path");
const fs = require("fs-extra");

const projectdir = (...paths) => path.join(__dirname, "../", ...paths);

fs.mkdirSync(projectdir("public"));
fs.copySync(projectdir("slides/images"), projectdir("public/images"));
