# 2022 年版 Go 入門 〜 ゆるネイティブでGo言語

## スライドはここで見ることができます

https://yknakamura.gitlab.io/2022-07_IntroductionToGolang/

このスライドは[Marp](https://marp.app/)で作成されています。

## スライドの実行方法

まず、依存ライブラリをインストールする。

```sh
# 依存性取得
yarn
```

### ローカルサーバー起動

スライド作成時など、ローカルでサーバーを起動してリアルタイム表示する場合。

```sh
# サーバー起動
yarn start
```

http://localhost:8080 で起動する。

### 静的なHTMLとして出力する場合

```sh
# HTMLで出力
yarn build
```

`public/`の中に生成される。

### PDFとして出力する場合

PDF出力の実行には、OSにChromium系のブラウザ（Google Chrome、 Chromium、Microsoft Edge）がインストールされている必要がある。

```sh
# PDFで出力
yarn pdf
```

`build/slide.pdf`に生成される。

## このスライドをベースにして新しいスライドを作る方法

### 必要なもの

- Git
- Node.js v16
  - (Optional) Node用のバージョン管理ツール
    `.node-version`が扱えるもの
- Yarn v1系

### 手順

- このリポジトリをcloneする
- `.git`を削除して、改めて自分のリポジトリを作成する
  - `git init`とかして、GitLabにリポジトリ作って、スライド書き直したら`push`する
- `package.json`の内容を書き換える
  - `name`: スライドのパッケージ名
  - `description`: スライドの概要
  - `author`: 著者
  - `homepage`: スライドのページ（リポジトリのページ）
    - このURLからQRコードが生成されるようになっている
  - `repository`: スライドのリポジトリ（GitでアクセスするURLであること）
- `slides/`の中身を全部消す
- `yarn`を実行して、依存ライブラリをインストールする
- `yarn generate:qrcode`を実行して、新しいQRコード画像を生成する
  - `package.json`の`homepage`を変更した際は、このコマンドで再生成する
- `slides/index.md`を改めて作って、スライドを書いていく
  - `yarn start`でサーバーを起動して、表示を確認しながら書くとよい
- `README.md`内のスライドのリンクを書き換える

スライドの書き方などはMarpのドキュメントを参照。

## 参考

- [Marp: Markdown Presentation Ecosystem](https://marp.app/)
- [marp-team/marp: The entrance repository of Markdown presentation ecosystem](https://github.com/marp-team/marp/)
- [Marp CLI](https://github.com/marp-team/marp-cli)
- [Marpit](https://marpit.marp.app/)
  - [Marpit Markdown](https://marpit.marp.app/markdown)

### Marpについて

MarpはMarkdownでスライドが作成できるオープンソースのプレゼンテーションツール。

HTMLの他にPDFやPowerPointでのエクスポートもできる。

#### MarpとかMarpitの関係

- Marpエコシステム
  - フレームワークとコアの部分
    - Marpit
    - Marp Core
  - ツール
    - Marp CLI
    - Marp for VS Code
